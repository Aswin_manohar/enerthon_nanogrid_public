# Enerthon Nanogrid


Enerthon Nanogrid Schneider Challenge to optimize households.


## Setup for local development

To set this project up for local development after cloning it from `git`, run

```bash
./scripts/setup
```

This will
- set up a virtual environment and install all dependencies with `pipenv`

## Run the dashboard in a local environment

```bash
pipenv run python -m enerthon_nanogrid.dashboard
```

This will
- run the dashboard on http://127.0.0.1:5000/
