"""Class to derive PV forecasts."""
import matplotlib.pyplot as plt
import os
import numpy as np
import pandas as pd
import pvlib

# from pvlib.pvsystem import PVSystem #, retrieve_sam
# from pvlib.tracking import SingleAxisTracker
from pvlib.modelchain import ModelChain

from enerthon_nanogrid.const import DATA_DIR
from enerthon_nanogrid.weather_data import WeatherData


class PVForecast(object):
    """Class with different PV forecasting methods."""

    def __init__(self, lat, lon):
        """Init class with lat and lon."""
        self.lat = lat
        self.lon = lon

    def pv_power_forecast(
        self,
        lat,
        lon,
        model,
        pv_module_name,
        inverter_name,
        surface_tilt,
        surface_azimuth,
        times,
        temp,
        windspeed,
        cloudcover,
        n_modules,
    ):
        """Add Docstring.
        Args:
            n_modules (int): number of available PV modules
        """
        # load the PV module and inverter specifications
        sandia_module = pvlib.pvsystem.retrieve_sam("SandiaMod")[pv_module_name]
        cec_inverter = pvlib.pvsystem.retrieve_sam("cecinverter")[inverter_name]

        # initialize location object
        location = pvlib.location.Location(latitude=lat, longitude=lon)

        # initialize pv system
        system = pvlib.pvsystem.PVSystem(
            surface_tilt=surface_tilt,
            surface_azimuth=surface_azimuth,
            module_parameters=sandia_module,
            inverter_parameters=cec_inverter,
        )

        location = pvlib.location.Location(
            lat, lon, tz="Europe/Berlin", name="Darmstadt"
        )

        sol_pos = location.get_solarposition(times)

        # initialise the weather / PV model
        mc = ModelChain(system, location)

        if model == "Liu-Jordan":
            irradiance_liujordan = self.irradiance_from_cloud_cover_transmittance_scaling_liujordan(
                location, sol_pos, cloudcover
            )
            weather = pd.DataFrame(
                {
                    "temp_air": temp,
                    "cloud_cover": cloudcover,
                    "wind_speed": windspeed,
                    "ghi": irradiance_liujordan["ghi"],
                    "dni": irradiance_liujordan["dni"],
                    "dhi": irradiance_liujordan["dhi"],
                }
            ).fillna(0)
        else:
            print(f"Model {model} not implemented!")

        # run the model and extract the results
        mc.run_model(times=sol_pos.index, weather=weather)
        # total_irrad = mc.total_irrad
        ac_power = mc.ac.fillna(0) * n_modules

        return ac_power

    def irradiance_from_cloud_cover_transmittance_scaling_liujordan(
        self, location, sol_pos, cloud_cover
    ):
        """Calculate the irradiance based on Liu-Jordan."""
        dni_extra = pvlib.irradiance.get_extra_radiation(sol_pos.index)
        airmass = location.get_airmass(sol_pos.index)
        offset = 0.75
        transmittance = ((100.0 - cloud_cover) / 100.0) * offset
        tau = transmittance
        dni = dni_extra * tau ** airmass["airmass_absolute"]
        dhi = (
            0.3
            * (1.0 - tau ** airmass["airmass_absolute"])
            * dni_extra
            * np.cos(np.radians(sol_pos["apparent_zenith"]))
        )
        ghi = dhi + dni * np.cos(np.radians(sol_pos["apparent_zenith"]))
        # irradiance = pvlib.irradiance.liujordan(sol_pos['apparent_zenith'], transmittance, airmass['airmass_absolute'], dni_extra=dni_extra)
        # irradiance = irradiance.fillna(0)
        irradiance = pd.DataFrame({"ghi": ghi, "dni": dni, "dhi": dhi}).fillna(0)
        return irradiance


if __name__ == "__main__":

    # parameters of the PV system and location
    pv_params = {
        "lat": 48.545146,  # latitude close to Passau
        "lon": 13.353054,  # longitude close to Passau
        "station_id": "K2596",  # ID of the DWD weather station
        "pv_module_name": "Canadian_Solar_CS5P_220M___2009_",
        "inverter_name": "ABB__MICRO_0_25_I_OUTD_US_208__208V_",
        "surface_tilt": 0,
        "surface_azimuth": 180,  # pvlib uses 0=North, 90=East, 180=South, 270=West convention
        "n_modules": 1,
    }

    # instance of the class
    pvf = PVForecast(pv_params["lat"], pv_params["lon"])

    # instance of the class
    wd = WeatherData(pv_params["lat"], pv_params["lon"])

    # get example weather data from openweather files (from the iBKM project)
    # this is for a specific location, independent of the lat/lon given above
    # contains some missing data!
    df_weather = wd.get_openweather_data(
        filename="OpenWeather_Actuals.csv",
        ilocation=0,
        columns=["temperature", "pressure", "wind_speed", "clouds_all"],
        start="2019-01-01",
        end="2020-01-01",
    )

    print("OpenWeather data")
    print(df_weather)

    # calculate model for PV power forecast
    cch_ac_power_hourly = pvf.pv_power_forecast(
        pv_params["lat"],
        pv_params["lon"],
        "Liu-Jordan",
        pv_params["pv_module_name"],
        pv_params["inverter_name"],
        pv_params["surface_tilt"],
        pv_params["surface_azimuth"],
        df_weather.index,  #    times
        df_weather["temperature"] - 273.15,  #    temp
        df_weather["wind_speed"],
        df_weather["clouds_all"],
        pv_params["n_modules"],
    )

    print("cch_ac_power_hourly: ", cch_ac_power_hourly)

    plt.plot(cch_ac_power_hourly)
    plt.show()
