"""Class to get weather data."""
import os
import numpy as np
import pandas as pd

from enerthon_nanogrid.const import DATA_DIR


class WeatherData(object):
    """Class with different weather APIs."""

    def __init__(self, lat, lon):
        """Init class with lat and lon."""
        self.lat = lat
        self.lon = lon

    def get_openweather_data(
        self,
        filename="OpenWeather_Actuals.csv",
        columns=["temperature", "pressure", "wind_speed", "clouds_all"],
        ilocation=0,
        start="2019-01-01",
        end="2020-01-01",
    ):
        """Read the weather data from OpenWeather, given as example.
            available columns:
            ['dt', 'dt_iso', 'timezone', 'city_name', 'lat', 'lon', 'temp',
             'feels_like', 'temp_min', 'temp_max', 'pressure', 'sea_level',
             'grnd_level', 'humidity', 'wind_speed', 'wind_deg', 'rain_1h',
             'rain_3h', 'snow_1h', 'snow_3h', 'clouds_all', 'weather_id',
             'weather_main', 'weather_description', 'weather_icon'
        """

        # available pairs of latitude and longitude in the provided open weather data file
        ow_locations_lat = [
            48.545146,
            50.798281,
            51.180254,
            51.43479,
            52.381287,
            53.045015,
            53.861708,
        ]
        ow_locations_lon = [
            13.353054,
            6.02439,
            8.489068,
            12.239622,
            13.062229,
            8.797904,
            8.126564,
        ]

        # read the data file
        file = os.path.join(DATA_DIR, filename)
        df = pd.read_csv(file, header=0)

        # select the location
        df = df[
            (df["lat"] == ow_locations_lat[ilocation])
            & (df["lon"] == ow_locations_lon[ilocation])
        ]

        time_arr = [t[:-10] for t in df["dt_iso"]]
        df["dt_iso"] = pd.to_datetime(time_arr)
        df.set_index("dt_iso", inplace=True)

        df.rename(columns={"temp": "temperature"}, inplace=True)

        # pressure from hPa to Pa
        df["pressure"] *= 100.0

        # restrict columns
        df = df[columns]

        # restrict date range
        df = df[
            ((df.index >= pd.to_datetime(start)) & (df.index < pd.to_datetime(end)))
        ]

        return df


if __name__ == "__main__":

    # parameters
    params = {
        "lat": 48.545146,  # latitude
        "lon": 13.353054,  # longitude
    }

    # instance of the class
    wd = WeatherData(params["lat"], params["lon"])

    # get example weather data from openweather files
    weather = wd.get_openweather_data(
        filename="OpenWeather_Actuals.csv",
        ilocation=1,
        columns=["temperature", "pressure", "wind_speed", "clouds_all"],
        start="2019-01-01",
        end="2020-01-01",
    )

    print("OpenWeather data")
    print(weather)
    print(np.max(weather["clouds_all"]))
    print(np.min(weather["clouds_all"]))
