"""Constants needed for the project."""

import os
from pathlib import Path


PROJECT_ROOT = Path(__file__).parent.parent.absolute()
DATA_DIR = os.path.join(PROJECT_ROOT, "data")
